![red mailbox logo](./logo.png)

# getmail_ms_oauth2
A shell script to get messages from Microsoft/Office365 hosted mailstores using the OAuth2 authorization code flow.

See also the relevant [MS documentation](https://learn.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow) 


## Prerequisites
- access to a mailbox hosted by Microsoft
- bash
- curl
- urlencode
- jq
- sendmail, or any other local mail delivery agent


## Configuration

1. edit the script to define things like your server hosted mailbox, your local mail delivery agent and your local mailbox

2. register a new app with Microsoft Azure (see script for instructions)

3. create a client secret, which will be used to access the new app (see script for instructions)

4. edit the script to add the new app configuration settings

5. grant the necessary permissions to the new app (see script for instructions)

6. authorize the new app (see script for instructions)

7. get access and refresh tokens (see script for instructions)


## Usage

```
$ getmail_ms_oauth2
```
or
```
$ getmail_ms_oauth2 --delete-downloaded-messages
```

The downloaded messages are delivered to a local mailbox.
Use the --delete-downloaded-messages option to delete messages from server after downloading.


## Credits
Mailbox logo: [flaticons.net](https://flaticons.net/custom.php?i=q8VjUlDTjDXzri0IXIv5UrQF0q6zGTw) 

